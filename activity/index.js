// Get Cube
let int = 2;
let cube = 3;
let getCube = int ** cube; 
console.log(`The cube of ${int} is ${getCube}`);
// address
let address = [258, "Washington Ave NW", "California", 90011];
let [houseNum, streetName, state, zipCode] = address;
console.log(`I live at ${houseNum} ${streetName}, ${state} ${zipCode}`);
// animal
let animal = {
	name: "Lolong",
	species: "Saltwater Crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
}
let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${measurement}.`)
// numbers
let numbers = [1, 2, 3, 4, 5]
numbers.forEach(function(number){
	console.log(number)
})
let sum = (a, b, c, d, e) => (a + b + c + d + e);
let reduceNumber = sum(1, 2, 3, 4, 5);
console.log(reduceNumber)
// Dog Class
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);
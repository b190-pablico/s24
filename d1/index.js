console.log("Hello World");

// SECTION - Exponent operator
// pre-es6
/*
	let/const variableName = Math.pw(number, exponent)
*/
let firstNum = Math.pow(8, 2);
console.log(firstNum);

// es6
/*
	let/const variableName = number ** exponent
*/
let secondNum = 8 ** 2;
console.log(secondNum);

// SECTION - Template Literals

let name = "John";

// pre-es6
let message = "Hello " + name + "! Welcome to programming!"
// Message without template literals: message
console.log('Message without template literals: ' + message)

// es6
// single-line
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message without template literals ${message}`);

// multiple-line
const anotherMessage = `
${name} won the math competition.
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.
`;

console.log(anotherMessage);

// miniactivity
let interestRate = .15;
let principal = 1000;
console.log(`Total interest of ${principal} is ${principal * interestRate}.
The interest rate is ${interestRate * 100}%`)

/*
	- Template literals allow us to wrtie strings with embedded JS expressions
	- expressions in general are any valid unit of code that resolves into a value
	- "${}" are used to include JS expressions in strings using template literals
*/

// SECTION - Array Destructuring
/*
	- allows us to unpack elements in arrays to distinct variables
	- allows us to name array elements with varaibles inseatd of using index numbers
	- helps with code readability
	- SYNTAX:
		let/const [varA, varB, varC] = arrayName

*/
const fullName = ['Juan', 'Dela', 'Cruz'];
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

// array destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
// using template literals
console.log(`Hello ${firstName} ${middleName} ${lastName}`)

let person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}`)

// objext destructuring
const {givenName, maidenName, familyName} = person;
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}`)

function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
};

getFullName(person);

let pet ={
	name: "Sydney",
	trick: "Sit",
	treat: "Litson Manok"
}
function myPet({name, trick, treat}){
	console.log(`${name}, ${trick}!`);
	console.log(`good ${trick}!`);
	console.log(`here is ${treat} for you!`)
}
myPet(pet);

// SECTION - Arrow Function

/*const hello = ()=>{
	console.log("Hello World");
};*/

const printFullName = (firstName, middleName, lastName) =>{
	console.log(`${firstName} ${middleName} ${lastName}`)
}
printFullName("Portgas", "D.", "Ace");

// arrow function with loops
// pre-arrow function
const students = ["John", "Jane", "Judy"];
students.forEach(function(student){
	console.log(`${student} is a student`);
})

const add = (x, y) =>{
	return x + y;
}
console.log(add(10,21))

const greet = (name = "User") =>{
	return `Good morning ${name}!`
}

console.log(greet("John"))

// SECTION = Class-based Object BluePrints
class car{
	constructor (brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};
// first instance
// using initializer and/or dot notation, create a car object using the Car class that we have created.
let myCar = new car();
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

const myNewCar = new car("Toyota", "Vios", 2021);
console.log(myNewCar)